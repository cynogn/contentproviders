package com.gautam.contentproviderviewing;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

/**
 * View Activity displays the data in listView using SimpleCursor adapter
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * @version 1
 * 
 */
public class ViewingActivity extends Activity {
	private ListView mListView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewing);
		/*
		 * contains all teh columns of the table
		 */
		String[] projection = { "_id", "EmployeeName", "RollNumber" };
		/*
		 * returns the rows and columns
		 */
		Cursor tutorials = this.managedQuery(Uri.parse("content://"
				+ "com.gautam.customcontentprovider.TutListProvider" + "/"
				+ "tutorials"), projection, null, null, null);
		/*
		 * Move the cursot to the start
		 */
		tutorials.moveToFirst();
		/*
		 * Columns that needs to be displayed
		 */
		String[] uiBindFrom = { "_id", "EmployeeName", "RollNumber" };
		/*
		 * Id's that needs to be dispalyed
		 */
		int[] uiBindTo = { R.id.textView1, R.id.textView2, R.id.textView3 };
		/*
		 * defining the custom adapter
		 */
		CursorAdapter adapter = new SimpleCursorAdapter(
				this.getApplicationContext(), R.layout.list_item, tutorials,
				uiBindFrom, uiBindTo);
		/*
		 * binding the lsit View
		 */
		mListView = (ListView) findViewById(R.id.listView1);
		/*
		 * setting addapter tot he lsit View
		 */
		mListView.setAdapter(adapter);
	}
}
